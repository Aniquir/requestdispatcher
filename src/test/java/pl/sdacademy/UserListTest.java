package pl.sdacademy;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by RENT on 2017-10-04.
 */
public class UserListTest {

    private UserList userList;

    @Before
    public void initialize(){
        userList = new UserList();
    }

    @Test
    public void testCorrectLogin(){
        Assert.assertTrue(userList.correctPassword("alaKot", "kkk23"));
    }
    @Test
    public void testIncorrectLogin(){
        Assert.assertFalse(userList.correctPassword("alaKot", "kkk24"));
    }
}
