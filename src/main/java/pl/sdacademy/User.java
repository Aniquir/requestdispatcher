package pl.sdacademy;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by RENT on 2017-10-04.
 */
@Data
@AllArgsConstructor
public class User {

    private String login;
    private String password;

}
