package pl.sdacademy;

import src.main.java.pl.sdacademy.HtmlGenerator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * http://dominisz.pl
 * 23.09.2017
 */
@WebServlet(value = "/logowanie")
public class LoginServlet extends HttpServlet {

    private UserList userList = new UserList();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        RequestDispatcher requestDispatcher;

        if(userList.correctPassword(username, password)){
            requestDispatcher = req.getRequestDispatcher("/welcome");
        } else {
            requestDispatcher = req.getRequestDispatcher("error.html");
        }
        requestDispatcher.forward(req, resp);
    }
}
