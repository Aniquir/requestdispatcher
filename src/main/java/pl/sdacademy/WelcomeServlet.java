package pl.sdacademy;

import src.main.java.pl.sdacademy.HtmlGenerator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by RENT on 2017-10-04.
 */
@WebServlet(value = "/welcome")
public class WelcomeServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        PrintWriter out = resp.getWriter();
        HtmlGenerator.generateHeader(out, "Welcome");
        out.println("" +
                "Welcome!<br><br>" + username);
        HtmlGenerator.generateFooter(out);
    }
}
