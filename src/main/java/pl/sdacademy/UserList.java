package pl.sdacademy;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-10-04.
 */

public class UserList {

    private List<User> users;

    public UserList() {
        users = new LinkedList<>();
        users.add(new User("alaKot", "kkk23"));
        users.add(new User("wiolaJJ", "12345"));
        users.add(new User("danuta34", "azxsdcvf"));
        users.add(new User("rovenaR", "12#edr"));
        users.add(new User("teo99", "haslopl"));
    }

    public boolean correctPassword(String login, String password){
        for (User user : users){
            if (user.getLogin().equals(login) && user.getPassword().equals(password)){
                return true;
            }
        }
        return false;
    }
}
